"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
import pandas as pd
import numpy as np
from scipy.sparse import hstack, csr_matrix
from sklearn.model_selection import train_test_split
import lightgbm as lgb

SEED = 23
import os

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="model_training",
                   level=logging.INFO)


class ModelTraining(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, xpresso_run_name, parameters_filename,
                 parameters_commit_id):

        super().__init__(name="ModelTraining",
                         run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)

        # if you have specified parameters_filename or parameters_commit_id,
        # the run parameters can be accessed from self.run_parameters
        """ Initialize all the required constants and data here """
        self.train_data_path = "/data/train.csv"
        self.test_data_path = "/data/test.csv"
        self.model_path = os.path.join(self.OUTPUT_DIR,"model.h5")

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
            identify the current run. It must be passed. While running as
            pipeline, xpresso automatically adds it.
        """
        try:
            super().start(xpresso_run_name=xpresso_run_name)
            # === Your start code base goes here ===

            ### $xpr_param_pipeline_job_import
            stopWords = set(stopwords.words('english'))
            print(stopWords)
            train = pd.read_csv(self.train_data_path)
            traindex = train.index

            test = pd.read_csv(self.test_data_path)
            testdex = test.index
            test_id = test.id.values
            import gc
            y = train.target.values
            train.drop(['target'], axis=1, inplace=True)
            df = pd.concat([train, test], axis=0)

            to_drop = ['id', 'location']
            df.drop(to_drop, axis=1, inplace=True)
            df.reset_index(drop=True, inplace=True)
            del train, test
            gc.collect()

            from sklearn.model_selection import KFold

            kf = KFold(n_splits=5, shuffle=False)
            cols = ['keyword']
            train_new = df.loc[traindex, :].copy()
            train_new['target'] = y

            for train_index, test_index in kf.split(train_new):
                X_tr, X_val = train_new.iloc[train_index], train_new.iloc[
                    test_index]
                for col in cols:
                    item_id_target_mean = X_tr.groupby(col).target.mean()
                    X_val[col + 'mean_enc'] = X_val[col].map(
                        item_id_target_mean)
                train_new.iloc[test_index] = X_val

            prior = y.mean()
            train_new.fillna(prior, inplace=True)

            # Calculate a mapping: {item_id: target_mean}
            train_new = df.loc[traindex, :].copy()
            train_new['target'] = y
            keyword_target_mean = train_new.groupby('keyword').target.mean()

            # In our non-regularized case we just *map* the computed means to the `item_id`'s
            df.loc[traindex, 'keyword_target_enc'] = df.loc[
                testdex, 'keyword'].map(keyword_target_mean)

            # Fill NaNs
            df['keyword_target_enc'].fillna(y.mean(), inplace=True)
            df.drop(['keyword'], axis=1, inplace=True)

            df_train = df.loc[traindex, :].copy()
            df_test = df.loc[testdex, :].copy()
            DISASTER_TWEETS = y == 1

            # Meta Text Features
            textfeats = ["text"]
            for cols in textfeats:
                df[cols] = df[cols].astype(str)
                df[cols] = df[cols].astype(str).fillna('nicapotato')  # FILL NA
                df[cols] = df[
                    cols].str.lower()
                df[cols + '_num_chars'] = df[cols].apply(
                    len)  # Count number of Characters
                df[cols + '_num_words'] = df[cols].apply(lambda comment: len(
                    comment.split()))  # Count number of Words
                df[cols + '_num_unique_words'] = df[cols].apply(
                    lambda comment: len(set(w for w in comment.split())))
                df[cols + '_words_vs_unique'] = df[cols + '_num_unique_words'] /df[cols + '_num_words'] * 100  # Count Unique Words

            print("\n[TF-IDF] Term Frequency Inverse Document Frequency Stage")
            stopWords = set(stopwords.words('english'))

            import numpy as np
            tfidf_para = {
                "stop_words": stopWords,
                "analyzer": 'word',
                "token_pattern": r'\w{1,}',
                "sublinear_tf": True,
                "dtype": np.float32,
                "norm": 'l2',
                "norm": 'l2',
                "smooth_idf": False
            }

            from sklearn.feature_extraction.text import TfidfVectorizer
            vect = TfidfVectorizer(ngram_range=(1, 2), max_features=17000,
                                   **tfidf_para)
            vect.fit(df.loc[traindex, :].text.values)
            ready_df = vect.transform(df.text.values)

            X = hstack([csr_matrix(df.iloc[traindex, 1:].values),
                        ready_df[0:traindex.shape[0]]])
            X_train, X_valid, y_train, y_valid = train_test_split(
                X, y, test_size=0.10, random_state=SEED)

            lgbm_params = {
                'task': 'train',
                'boosting_type': 'gbdt',
                'objective': 'binary',
                'metric': ['auc', 'f1'],
                'max_depth': 16,
                'num_leaves': 37,
                'feature_fraction': 0.6,
                'bagging_fraction': 0.8,
                # 'bagging_freq': 5,
                'learning_rate': 0.019,
                'verbose': 0
            }

            # LGBM Dataset Formatting
            lgtrain = lgb.Dataset(X_train, y_train)  # ,

            lgvalid = lgb.Dataset(X_valid, y_valid)  # ,

            from sklearn.metrics import f1_score

            def lgb_f1_score(y_hat, data):
                y_true = data.get_label()
                y_hat = np.round(y_hat)  # scikits f1 doesn't like probabilities
                return 'f1', f1_score(y_true, y_hat), True

            evals_result = {}

            lgb_clf = lgb.train(
                lgbm_params,
                lgtrain,
                num_boost_round=200,
                valid_sets=[lgtrain, lgvalid],
                valid_names=['train', 'valid'],
                early_stopping_rounds=200,
                verbose_eval=200,
                feval=lgb_f1_score, evals_result=evals_result)
            import joblib
            # save model
            joblib.dump(lgb_clf, '/output/model.pkl')

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    run_name = ""
    params_filename = None
    params_commit_id = None
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    if len(sys.argv) >= 4:
        params_filename = sys.argv[2] if sys.argv[2] != "None" else None
        params_commit_id = sys.argv[3] if sys.argv[3] != "None" else None

    pipeline_job = ModelTraining(run_name, params_filename, params_commit_id)
    pipeline_job.start(xpresso_run_name=run_name)